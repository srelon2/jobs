<?php

namespace App\Http\Controllers\Admin;

use Auth;
use DB;
use Input;
use Redirect;
use Form;
use App\Language;
use App\Country;
use App\State;
use App\City;
use App\Helpers\MiscHelper;
use App\Helpers\DataArrayHelper;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use DataTables;
use App\Http\Requests\CityFormRequest;
use App\Http\Controllers\Controller;
use App\Traits\CountryStateCity;

class CityController extends Controller
{

    use CountryStateCity;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function indexCities()
    {
        $date= [];
        $date['4215']['ru']= [
            'Симферополь', 'Евпатория', 'Керчь', 'Партенит', 'Плодовое', 'Феодосия', 'Щёлкино', 'Ялта'
        ];
        $date['4203']['ru']= [
            'Киев',  
            'Белая Церковь',
            'Борисполь',
            'Бровары',
            'Вышгород',
            'Припять',
        ];
        $date['4211']['ru']= [
            'Севастополь'
        ];
        $date['4222']['ru']= [
            'Винница', 'Балановка', 'Ладыжин', 'Волынская область', 'Луцк', 'Владимир-Волынский', 'Ковель', 'Нововолынск'
        ];
        $date['4204']['ru']= [
            'Днепропетровск', 'Булаховка', 'Днепродзержинск', 'Жёлтые Воды', 'Зеленодольск', 'Кривой Рог', 'Кринички', 'Марганец', 'Никополь', 'Новомосковск', 'Павлоград'
        ];
        $date['4205']['ru']= [
          'Донецк', 'Андреевка',
'Артёмовск',
'Безимянное',
'Белосарайская Коса',
'Бересток',
'Волноваха',
'Горловка',
'Енакиево',
'Зугрес',
'Константиновка',
'Краматорск',
'Красноармейск',
'Курахово',
'Макеевка',
'Мариуполь',
'Николаевка',
'Райгородок',
'Светлодарск',
'Святогорск',
'Славянск',
'Снежное',
'Торез',
'Шахтёрск',
        ];
        $date['4225']['ru']= [
            'Житомир',
'Андреевка',
'Бердичев',
'Коростень',
'Новоград-Волынский',
        ];
        $date['4226']['ru']= [
            'Ужгород',
'Берегово',
'Виноградов',
'Иршава',
'Мукачево',
'Рахов',
'Свалява',
'Тячев',
'Хуст',
        ];
        $date['4206']['ru']= [
           'Запорожье',
'Бердянск',
'Днепрорудное',
'Камыш-Заря',
'Мелитополь',
'Токмак',
'Энергодар', 
        ];
        $date['4219']['ru']= [
            'Ивано-Франковск',
'Бурштын',
'Калуш',
'Коломыя',
        ];
        $date['4217']['ru']= [
            'Кировоград',
            'Александрия',
            'Луганск',
            'Алчевск',
            'Антрацит',
            'Белолуцк',
            'Ирмно',
            'Краснодон',
            'Красный Луч',
            'Лисичанск',
            'Ровеньки',
            'Рубежное',
            'Свердловск',
            'Северодонецк',
            'Старобельск',
            'Стаханов',
            'Счастье',
            'Чернухино',
        ];
        $date['4214']['ru']= [
            'Львов',
            'Дрогобыч',
            'Красное',
            'Стрый',
            'Червоноград',
        ];
        $date['4212']['ru']= [
        'Николаев',
        'Вознесенск',
        'Луч',
        'Первомайск',
        'Южноукраинск',    
        ];
        $date['4209']['ru']= [
            'Одесса',
            'Белгород-Днестровский',
            'Жовтень',
            'Измаил',
            'Ильичевск',
            'Каменское',
            'Орловка',
            'Петровка',
        ];
        $date['4207']['ru']= [
            'Полтава',
            'Красногоровка',
            'Кременчуг',
            'Лубны',
        ];
        $date['4221']['ru']= [
           'Ровно',
            'Антополь',
            'Кузнецовск', 
        ];
        $date['4216']['ru']= [
            'Сумы',
            'Ахтырка',
            'Белополье',
            'Конотоп',
            'Ромны',
            'Шостка',
        ];
        $date['4228']['ru']= [
            'Тернополь',
            'Лозовая',   
        ];
        $date['4208']['ru']= [
            'Харьков',
            'Барвенково',
            'Змиёв',
            'Изюм',
            'Кегичёвка',
            'Комсомольское',
            'Мерефа',
            'Лозовая',
            'Подворки',
            'Тарановка',
        ];
        $date['4223']['ru']= [
            'Херсон',
            'Васильевка',
            'Геническ',
            'Большая Александровка',
            'Новая Каховка',
            'Новотроицкое',
            'Рыбальче',
            'Чаплинка',
        ];
        $date['4224']['ru']= [
            'Хмельницкий',
            'Волочиск',
            'Каменец-Подольский',
            'Нетешин',
        ];
        $date['4213']['ru']= [
            'Черкассы',
            'Буки',
            'Смела',
            'Умань',
        ];
        $date['4218']['ru']= [
            'Чернигов',
            'Козелець',
            'Крути',
            'Нежин',
            'Новгород-Северский',
            'Прилуки',
        ];
        $date['4227']['ru']= [
            'Черновцы'
        ];
        
        
        
        $date['4215']['ua']= [
            "Сімферополь", "Євпаторія", "Керч", "Партеніт", "Плодове", "Феодосія", "Щолкіно", "Ялта"
        ];
        $date['4203']['ua']= [
         'Київ',
             'Біла Церква',
             'Бориспіль',
             'Бровари',
             'Вишгород',
             "Прип'ять",
        ];
        $date['4211']['ua']= [
            'Севастополь'
        ];
        $date['4222']['ua']= [
            "Вінниця", "Баланівка", "Ладижин", "Волинська область", "Луцьк",
            "Володимир-Волинський","Ковель","Нововолинськ",
        ];
        $date['4204']['ua']= [
            'Дніпропетровськ', 'Булахівка', 'Дніпродзержинськ', 'Жовті Води', 'Зеленодольськ', 'Кривий Ріг', 'Кринички', 'Марганець', 'Нікополь', 'Новомосковськ', 'Павлоград'
        ];
        $date['4205']['ua']= [
          'Донецьк', 'Андріївка',
'Артемівськ',
'Безімянное',
'Білосарайська Коса',
'Бересток',
'Волноваха',
'Горлівка',
'Єнакієве',
'Зугрес',
'Костянтинівка',
'Краматорськ',
'Красноармійськ',
'Курахово',
'Макіївка',
'Маріуполь',
'Миколаївка',
'Райгородок',
'Світлодарськ',
'Святогірськ',
"Слов'янськ",
'Сніжне',
'Торез',
'Шахтарськ',
        ];
        $date['4225']['ua']= [
            'Житомир',
'Андріївка',
'Бердичів',
'Коростень',
'Новоград-Волинський',
        ];
        $date['4226']['ua']= [
            'Ужгород',
'Берегово',
'Виноградов',
'Іршава',
'Мукачево',
'Рахів',
'Свалява',
'Тячів',
'Хуст',
        ];
        $date['4206']['ua']= [
           'Запоріжжя',
'Бердянськ',
'Дніпрорудне',
'Комиш-Зоря',
'Мелітополь',
'Токмак',
'Енергодар',
        ];
        $date['4219']['ua']= [
            'Івано-Франківськ',
'Бурштин',
'Калуш',
'Коломия',
        ];
        $date['4217']['ua']= [
          'Кіровоград',
             'Олександрія',
             'Луганськ',
             'Алчевськ',
             'Антрацит',
             'Білолуцьк',
             'Ірмно',
             'Краснодон',
             'Червоний луч',
             'Лисичанськ',
             'Ровеньки',
             'Рубіжне',
             'Свердловськ',
             'Сєвєродонецьк',
             'Старобільськ',
             'Стаханов',
             'Щастя',
             'Чорнухине',
        ];
        $date['4214']['ua']= [
      'Львів',
             'Дрогобич',
             'Червоне',
             'Стрий',
             'Червоноград',
        ];
        $date['4212']['ua']= [
       'Миколаїв',
         'Вознесенськ',
         'Луч',
         'Первомайськ',
         'Южноукраїнськ',   
        ];
        $date['4209']['ua']= [
            'Одеса',
             'Білгород-Дністровський',
             'Жовтень',
             'Ізмаїл',
             'Іллічівськ',
             "Кам'янське",
             'Орловка',
             'Петрівка',
        ];
        $date['4207']['ua']= [
          'Полтава',
             'Красногорівка',
             'Кременчук',
             'Лубни',
        ];
        $date['4221']['ua']= [
'Рівне',
             'Антопіль',
             'Кузнецовськ',
        ];
        $date['4216']['ua']= [
            'Суми',
             'Охтирка',
             'Білопілля',
             'Конотоп',
             'Ромни',
             'Шостка',
        ];
        $date['4228']['ua']= [
            'Тернопіль',
             'Лозова',
        ];
        $date['4208']['ua']= ['Харків',
             'Барвінкове',
             'Зміїв',
             'Ізюм',
             'Кегичівка',
             'Комсомольське',
             'Мерефа',
             'Лозова',
             'Подвірки',
             'Таранівка',
        ];
        $date['4223']['ua']= ['Херсон',
             'Василівка',
             'Генічеськ',
             'Велика Олександрівка',
             'Нова Каховка',
             'Новотроїцьке',
             'Рибальче',
             'Чаплинка',
        ];
        $date['4224']['ua']= ['Хмельницький',
             'Волочиськ',
             "Кам'янець-Подільський",
             'Нетішин',
        ];
        $date['4213']['ua']= ['Черкаси',
             'Буки',
             'Сміла',
             'Умань',
        ];
        $date['4218']['ua']= ['Чернігів',
             'Козелець',
             'Крути',
             'Ніжин',
             'Новгород-Сіверський',
             'Прилуки',
        ];
        $date['4227']['ua']= [
            'Чернівці'
        ];
        foreach($date as $keys=>$values) {
            $i= 0;
            foreach($values['ua'] as $key=> $value) {
                $item= new City();
                $item->city= $value;
                $item->state_id= $keys;
                $item->is_default= 1;
                $item->is_active= 1;
                $item->sort_order= $i;
                $item->lang= 'ua';
                $item->save();
                $item->city_id= $item->id;
                $item->save();
                
                $ru= new City();
                $ru->city= $date[$keys]['ru'][$key];
                $ru->city_id= $item->id;
                $ru->state_id= $keys;
                $ru->is_default= 1;
                $ru->is_active= 1;
                $ru->sort_order= $i;
                $ru->lang= 'ru';
                $ru->save();
                
                $i++;
            }
        }
        dd($date);
        $languages = DataArrayHelper::languagesNativeCodeArray();
        $countries = DataArrayHelper::defaultCountriesArray();
        $states = array(''=>'Select State');
        return view('admin.city.index')
                        ->with('languages', $languages)
                        ->with('countries', $countries)
                        ->with('states', $states);
    }

    public function createCity()
    {
        $languages = DataArrayHelper::languagesNativeCodeArray();
        $countries = DataArrayHelper::defaultCountriesArray();
        $states = array(''=>'Select State');
        return view('admin.city.add')
                        ->with('languages', $languages)
                        ->with('countries', $countries)
                        ->with('states', $states);
    }

    public function storeCity(CityFormRequest $request)
    {
        $city = new City();
        $city->id = $request->input('id');
        $city->lang = $request->input('lang');
        $city->state_id = $request->input('state_id');
        $city->city = $request->input('city');
        $city->is_default = $request->input('is_default');
        $city->city_id = $request->input('city_id');
        $city->is_active = $request->input('is_active');
        $city->save();
        /*         * ************************************ */

        $city->sort_order = $city->id;

        if ((int) $request->input('is_default') == 1) {
            $city->city_id = $city->id;
        } else {
            $city->city_id = $request->input('city_id');
        }

        $city->update();
        /*         * ************************************ */

        flash('City has been added!')->success();
        return \Redirect::route('edit.city', array($city->id));
    }

    public function editCity($id)
    {
        $languages = DataArrayHelper::languagesNativeCodeArray();
        $countries = DataArrayHelper::defaultCountriesArray();
        $states = array(''=>'Select State');
        $city = City::findOrFail($id);
        return view('admin.city.edit')
                        ->with('city', $city)
                        ->with('languages', $languages)
                        ->with('countries', $countries)
                        ->with('states', $states);
    }

    public function updateCity($id, CityFormRequest $request)
    {
        $city = City::findOrFail($id);
        $city->id = $request->input('id');
        $city->lang = $request->input('lang');
        $city->state_id = $request->input('state_id');
        $city->city = $request->input('city');
        $city->is_default = $request->input('is_default');
        $city->city_id = $request->input('city_id');
        $city->is_active = $request->input('is_active');
        /*         * ************************************ */
        if ((int) $request->input('is_default') == 1) {
            $city->city_id = $city->id;
        } else {
            $city->city_id = $request->input('city_id');
        }
        /*         * ************************************ */
        $city->update();

        flash('City has been updated!')->success();
        return \Redirect::route('edit.city', array($city->id));
    }

    public function fetchCitiesData(Request $request)
    {
        $cities = City::select([
                    'cities.id', 'cities.lang', 'cities.state_id', 'cities.city', 'cities.is_default', 'cities.city_id', 'cities.is_active',
                ])->sorted();
        return Datatables::of($cities)
                        ->filter(function ($query) use ($request) {

                            if ($request->has('id') && !empty($request->id)) {
                                $query->where('cities.id', 'like', "{$request->get('id')}%");
                            }

                            if ($request->has('lang') && !empty($request->lang)) {
                                $query->where('cities.lang', 'like', "{$request->get('lang')}");
                            }

                            if ($request->has('country_id') && !empty($request->country_id)) {
                                $state_ids = State::select('states.state_id')->where('states.country_id', '=', $request->country_id)->isDefault()->active()->sorted()->pluck('states.state_id')->toArray();

                                $query->whereIn('cities.state_id', $state_ids);
                            }

                            if ($request->has('state_id') && !empty($request->state_id)) {
                                $query->where('cities.state_id', '=', "{$request->get('state_id')}");
                            }

                            if ($request->has('city') && !empty($request->city)) {
                                $query->where('cities.city', 'like', "%{$request->get('city')}%");
                            }

                            if ($request->has('is_active') && $request->is_active != -1) {
                                $query->where('cities.is_active', '=', "{$request->get('is_active')}");
                            }
                        })
                        ->addColumn('state_id', function ($cities) {
                            return $cities->getState('state') . ' - ' . $cities->getCountry('country');
                        })
                        ->addColumn('city', function ($cities) {
                            $city = str_limit($cities->city, 100, '...');
                            $direction = MiscHelper::getLangDirection($cities->lang);
                            return '<span dir="' . $direction . '">' . $city . '</span>';
                        })
                        ->addColumn('action', function ($cities) {
                            /*                             * ************************* */
                            $activeTxt = 'Make Active';
                            $activeHref = 'makeActive(' . $cities->id . ');';
                            $activeIcon = 'square-o';
                            if ((int) $cities->is_active == 1) {
                                $activeTxt = 'Make InActive';
                                $activeHref = 'makeNotActive(' . $cities->id . ');';
                                $activeIcon = 'check-square-o';
                            }
                            return '
				<div class="btn-group">
					<button class="btn blue dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action
						<i class="fa fa-angle-down"></i>
					</button>
					<ul class="dropdown-menu">
						<li>
							<a href="' . route('edit.city', ['id' => $cities->id]) . '"><i class="fa fa-pencil" aria-hidden="true"></i>Edit</a>
						</li>						
						<li>
							<a href="javascript:void(0);" onclick="deleteCity(' . $cities->id . ', ' . $cities->is_default . ');" class=""><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a>
						</li>
						<li>
						<a href="javascript:void(0);" onClick="' . $activeHref . '" id="onclickActive' . $cities->id . '"><i class="fa fa-' . $activeIcon . '" aria-hidden="true"></i>' . $activeTxt . '</a>
						</li>																																		
					</ul>
				</div>';
                        })
                        ->rawColumns(['action', 'state_id', 'city'])
                        ->setRowId(function($cities) {
                            return 'cityDtRow' . $cities->id;
                        })
                        ->make(true);
        //$query = $dataTable->getQuery()->get();
        //return $query;
    }

    public function makeActiveCity(Request $request)
    {
        $id = $request->input('id');
        try {
            $city = City::findOrFail($id);
            $city->is_active = 1;
            $city->update();
            echo 'ok';
        } catch (ModelNotFoundException $e) {
            echo 'notok';
        }
    }

    public function makeNotActiveCity(Request $request)
    {
        $id = $request->input('id');
        try {
            $city = City::findOrFail($id);
            $city->is_active = 0;
            $city->update();
            echo 'ok';
        } catch (ModelNotFoundException $e) {
            echo 'notok';
        }
    }

    public function sortCities()
    {
        $languages = DataArrayHelper::languagesNativeCodeArray();
        return view('admin.city.sort')->with('languages', $languages);
    }

    public function citySortData(Request $request)
    {
        $lang = $request->input('lang');
        $cities = City::select('cities.id', 'cities.city', 'cities.sort_order')
                ->where('cities.lang', 'like', $lang)
                ->orderBy('cities.sort_order')
                ->get();
        $str = '<ul id="sortable">';
        if ($cities != null) {
            foreach ($cities as $city) {
                $str .= '<li id="' . $city->id . '"><i class="fa fa-sort"></i>' . $city->city . '</li>';
            }
        }
        echo $str . '</ul>';
    }

    public function citySortUpdate(Request $request)
    {
        $cityOrder = $request->input('cityOrder');
        $cityOrderArray = explode(',', $cityOrder);
        $count = 1;
        foreach ($cityOrderArray as $cityId) {
            $city = City::find($cityId);
            $city->sort_order = $count;
            $city->update();
            $count++;
        }
    }

}
